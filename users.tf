###################################
# CREATE IAM USER ACCOUNT
##################################

resource "aws_iam_user" "username" {
  name = var.username
}

// SET USER PASSWORD
resource "aws_iam_user_login_profile" "password" {
  count   = var.console_access == true ? 1 : 0
  user    = aws_iam_user.username.name
  pgp_key = "keybase:${var.keybase_username}"
  password_reset_required = true
}

// ENABLE ACCESS & SECRET KEY
resource "aws_iam_access_key" "api" {
  count   = var.api_access == true ? 1 : 0
  user    = aws_iam_user.username.name
  pgp_key = "keybase:${var.keybase_username}"
}


// PRIVILAGES
data "aws_iam_policy" "policy" {
  for_each = var.policies == null ? null : toset(var.policies)
  name = each.key
}

// add policies listed in the var.policies to the created user
resource "aws_iam_user_policy_attachment" "policy-attach" {
  for_each = var.policies == null ? null : toset(var.policies)
  user       = aws_iam_user.username.name
  policy_arn = data.aws_iam_policy.policy[each.key].arn
}


###################################
# VARIABLES
##################################

variable "username" {
  description = "account username"
  type = string
  default = "kraado-tech0"
}

variable "keybase_username" {
  description = "keybase account name"
  type    = string
  default = "kraado_user1"
}

variable "console_access" {
  description = "enable or disable console access"
  type    = bool
  default = true
}

variable "api_access" {
  description = "enable or disable api access"
  type    = bool
  default = true
}

variable "policies" {
  description = "user permission"
  type = list(string)
  default = [ "IAMReadOnlyAccess", "IAMUserChangePassword", "AmazonEC2FullAccess", "AmazonVPCFullAccess" ]
}

variable "region" {
  type = string
  default = "eu-west-1"
}

###################################
# CREDENTIAL DETAILS OUTPUT
##################################

output "credential" {
  sensitive = false
  value = {
    username = aws_iam_user.username.name
    console_password = var.console_access == true && var.keybase_username != "" ? aws_iam_user_login_profile.password[0].encrypted_password : var.console_access == true && var.keybase_username == "" ? aws_iam_user_login_profile.password[0].password : null
    access_key = var.api_access == true  ? aws_iam_access_key.api[0].id : null
    secret_key = var.api_access == true && var.keybase_username != "" ? aws_iam_access_key.api[0].encrypted_secret : var.api_access == true && var.keybase_username == "" ? aws_iam_access_key.api[0].secret : null

  }
}

